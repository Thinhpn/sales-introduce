﻿using sales_introduce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace sales_introduce.Controllers
{
    public class SalesController : ApiController
    {
        [HttpGet]
        public StoredProcedureResult<sp_getListSalesInfor_Result> Get()
        //[FromUri] Filter value
        {
            using (var context = new SALES_INFORMATIONEntities())
            {
                context.Configuration.ProxyCreationEnabled = false;
                StoredProcedureResult<sp_getListSalesInfor_Result> result = new StoredProcedureResult<sp_getListSalesInfor_Result>();
                result.Items = context.sp_getListSalesInfor().ToList();
                result.Count = (int)result.Items.Count();
                return result;
            }
            
        }
    }

    public class Filter
    {
        public int count_index { get; set; }

    }

    public class StoredProcedureResult<T>
    {
        public IEnumerable<T> Items { get; set; }
        public int Count { get; set; }
    }
}
